= Exercici 1: Grafana
:icons: font
:source-highlighter: pygments
:pygments-style: igor
:imagesdir: ./assets/img/
:toc:
:toc-title: Index

NOTE: He afegit tornat a fer el proces d'instalacio del postgres, amb el meu ordinador per afegir las captures que em faltaven, ja que la practica la tenia feta en el ordinador
de classe abans que haguessin problemes amb els del mati. +
Es possible que vegis captures noves i captures de quan la vaig fer a classe, unes estan fetes amb el fedora 32 i les altres desde
pop os

<<<

== PostgreSQL

=== Instalacio
.script de la pagina principal de postgres
----
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get -y install postgresql
----

<<<

=== Creacio base de dades
.creacio d'usuari de grafana de base de dades i de les taules
image::postgres_creacio_base.png[]

<<<

=== Carregar dades
.carregar les dades a la taula
image::postgres_carregar_dades.png[]

.exemple sentencia: SELECT nombre, telefono, direccion FROM ocupacion;
image::dades.png[]

<<<

== Grafana

.grafana
image::grafana.png[]

.config
image::grafana_config.png[]

.query
image::grafana_query.png[]
