# PRACTIQUES M10

## Activitats telematiques
* [UF1.PR1. Cas pràctic](./exercicis_UF1/exercici1)  
* [UF1.PR1. Exercicis BI - Grafana / MongoCloud](./exercicis_UF1/exercici2)  
* [UF1.PR3. Odoo-Configuració d'una empresa i un mòdul](./exercicis_UF1/exercici3)  
* [UF1.PR4. Odoo II. Compres, vendes i producció](./exercicis_UF1/exercici4)  
